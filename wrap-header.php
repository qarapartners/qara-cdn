<!doctype html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="mobile-web-app-capable" content="no">
    <meta name="apple-mobile-web-app-capable" content="no">
    <title>Qara Partners: Assets</title>

    <meta name="robots" content="noindex,follow"/>

    <link rel='dns-prefetch' href='//use.typekit.net' />
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//code.jquery.com' />
    <link rel="stylesheet" href="/app/themes/qara-theme/dist/styles/main.css?v=1.0.1">
    <script src="//use.typekit.net/kln3dkp.js?ver=4.8"></script>

    <script>
      (function(d) {
        var tkTimeout=3000;
        if(window.sessionStorage){if(sessionStorage.getItem('useTypekit')==='false'){tkTimeout=0;}}
        var config = {
          kitId: 'kln3dkp',
          scriptTimeout: tkTimeout
        },
        h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";if(window.sessionStorage){sessionStorage.setItem("useTypekit","false")}},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='//use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
      })(document);
    </script>

    <style>
      img {
        box-shadow: 0 2px 30px rgba(155,155,155,.2);
        max-width: 150%;
      }
      ul {
        list-style: none;
        margin: 0;
        padding: 0;
      }
      li {
        margin: 0 0 60px;
        padding: 0;
      }
      li span {
        display: block;
        font-weight: 500;
        margin-bottom: 15px;
        opacity: .5;
      }
      li a {
        color: inherit;
        font-size: 16px;
      }
    </style>

  </head>

  <body ontouchstart="" class="home cdn page qarapartners">

    <nav class="" role="navigation" id="nav-offcanvas">

  <div class="sidenav-contact">
    <p><a class="navbar-brand" href="http://batc.org">Qara Partners</a></p>
  </div>

  <ul class="">
      </ul>

</nav>

    <div id="surround">
      <div class="non-footer">


<header class="banner" role="banner">
  <a class="navbar-brand brand" href="/">Qara Partners</a>

  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-offcanvas">
    <span class="bars">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </span>
  </button>

  <nav class="nav-primary collapse navbar-collapse" role="navigation" id="nav">
    <ul class="list-inline menu-main nav nav-list" role="tablist">
          <li class="nav-main-item  menu-item menu-item-type-custom menu-item-object-custom menu-item-50"><a class="nav-main-link" href="/#about">About</a>
            </li>
          <li class="nav-main-item  menu-item menu-item-type-custom menu-item-object-custom menu-item-48"><a class="nav-main-link" href="/#team">Our Team</a>
            </li>
          <li class="nav-main-item  menu-item menu-item-type-custom menu-item-object-custom menu-item-49"><a class="nav-main-link" href="/#library">Library</a>
            </li>
        </ul>
  </nav>
</header>


        <div class="wrap " role="document">


          <div class="content ">


            <main class="">

  <section id="images">

    <div class="intro">
      <p>Image assets for Qara Partners use.</p>
    </div>


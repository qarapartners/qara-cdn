<?php
require_once('wrap-header.php');
echo '<ul>';

$dir_open = opendir('.');
while ( false !== ( $filename = readdir($dir_open) ) ) {
  if ( (strpos($filename, ".") != 0) && (strpos($filename, "wrap") !== 0) && ($filename != basename(__FILE__)) ) {
    $link = "<li><a href='./$filename'><span>$filename</span><img src='./$filename' alt=''></a></li>";
    echo $link;
  }
}
closedir($dir_open);

echo '</ul>';

require_once('wrap-footer.php');
?>

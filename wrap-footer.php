
  </section>

            </main>
                      </div>
        </div>
      </div><!-- non-footer -->

      <footer class="content-info">
  <p class="copyright">&copy; <?php echo date('Y'); ?> Qara Partners, LLC. All rights reserved.</p>
  <p class="contact"><a href="mailto:hello@qarapartners.com">hello@qarapartners.com</a></p>
</footer>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>(window.jQuery && jQuery.noConflict()) || document.write('<script src="/wp/wp-includes/js/jquery/jquery.js"><\/script>')</script>
<script src="/app/themes/qara-theme/dist/scripts/main.js?v=1.0.0"></script>


    </div><!-- #surround -->

  </body>

</html>
